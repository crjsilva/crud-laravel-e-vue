<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'id'   => 1,
            'name' => 'Eletrônicos',
        ]);

        DB::table('categories')->insert([
            'id'   => 2,
            'name' => 'Eletrodomésticos',
        ]);

        DB::table('categories')->insert([
            'id'   => 3,
            'name' => 'Móveis',
        ]);

    }
}
