<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::with(['category'])->get();

        return json_encode($products);
    }

    public function create()
    {
        $categories = Category::orderBy('name')
            ->get()
            ->pluck('name', 'id');

        return view('create', ['categories' => json_encode($categories)]);
    }

    public function edit($id)
    {
        $product    = Product::find($id);
        $categories = Category::orderBy('name')
            ->get()
            ->pluck('name', 'id');

        return view('create', [
            'categories' => json_encode($categories),
            'data'       => json_encode($product),
        ]);
    }

    public function store(Request $request)
    {
        try {
            Product::create([
                'name'        => $request->name,
                'category_id' => $request->category_id,
                'quantity'    => $request->quantity,
                'price'       => $request->price,
            ]);

            return (['success' => '1']);
        } catch (Exception $e) {
            return (['success' => '0']);

        }
    }

    public function update(Request $request, $id)
    {
        try {
            Product::find($id)->update([
                'name'        => $request->data["name"],
                'category_id' => $request->data["category_id"],
                'quantity'    => $request->data["quantity"],
                'price'       => $request->data["price"],
            ]);

            return (['success' => '1']);
        } catch (Exception $e) {
            return (['success' => '0']);

        }
    }

    public function destroy($id)
    {
        Product::find($id)->delete();
    }
}
